/***************************************************
  Defs.h
	
****************************************************/

#ifndef _defs_
#define _defs_

#define TRUE 1
#define FALSE 0
#define NULL 0

#define KEYBOARD_MODE	0
#define MOUSE_MODE	1	

#define byte unsigned char
#define word short int
#define dword int

#define SYS_READ	3
#define SYS_WRITE	4
#define SYS_WRITE_AT	5
#define SYS_KEYBOARD	6
#define SYS_TERM	12
#define SYS_PROMPT	13
#define SYS_RTC_GET	24
#define SYS_RTC_SET	25
#define SYS_SCRSV	32
#define SYS_CHMODE	33
#define SYS_CLRSCRN	34

#define STDIN		0

#define STDOUT		0
#define ST_MOUSE 	1
#define CHAR_AT		2

#define GET_X		0
#define GET_Y		1
#define GET_L_BOT	2
#define GET_R_BOT	3
#define GET_M_BOT	4

#define DELETE 		0
#define	NEW_LINE	1
#define SHOW_CHAR	2

/* Flags para derechos de acceso de los segmentos */
#define ACS_PRESENT     0x80            /* segmento presente en memoria */
#define ACS_CSEG        0x18            /* segmento de codigo */
#define ACS_DSEG        0x10            /* segmento de datos */
#define ACS_READ        0x02            /* segmento de lectura */
#define ACS_WRITE       0x02            /* segmento de escritura */
#define ACS_IDT         ACS_DSEG
#define ACS_INT_386 	0x0E		/* Interrupt GATE 32 bits */
#define ACS_INT         ( ACS_PRESENT | ACS_INT_386 )


#define ACS_CODE        (ACS_PRESENT | ACS_CSEG | ACS_READ)
#define ACS_DATA        (ACS_PRESENT | ACS_DSEG | ACS_WRITE)
#define ACS_STACK       (ACS_PRESENT | ACS_DSEG | ACS_WRITE)

#pragma pack (1) 		/* Alinear las siguiente estructuras a 1 byte */

/* Descriptor de segmento */
typedef struct {
  word limit,
       base_l;
  byte base_m,
       access,
       attribs,
       base_h;
} DESCR_SEG;


/* Descriptor de interrupcion */
typedef struct {
  word      offset_l,
            selector;
  byte      cero,
            access;
  word	    offset_h;
} DESCR_INT;

/* IDTR  */
typedef struct {
  word  limit;
  dword base;
} IDTR;


#endif

