#define NOT_PRINTABLE		0
#define KEYBOARD_BUFFER_SIZE	256
#define BACKSPACE		8
#define CAPSLOCK		58
#define ENTER			10
#define LEFT_SHIFT_PRESSED	42
#define LEFT_SHIFT_RELEASED	170
#define RIGHT_SHIFT_PRESSED	54
#define RIGHT_SHIFT_RELEASED	182
#define SPACE			57

typedef struct{
	unsigned int canRead;
	unsigned int bufferIndexR;
	unsigned int bufferIndexW;
	char caps;
	char shifted;
	unsigned char buffer[KEYBOARD_BUFFER_SIZE];
} Keyboard;

void initialize_keyboard();
unsigned int can_read();
unsigned char read_from_buffer();
void process_scancode(int scancode);
int character_read(char* c);
