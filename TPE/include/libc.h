#include "../include/defs.h"

void setup_IDT_entry (DESCR_INT *item, byte selector, dword offset, byte access,byte cero) ;
int read(int fd, void* buffer, int size);
int write(int fd, void* buffer, int size);
int scanf(char* buffer, int size);
int getc();
void putc(char c);
void printf(char* string);
int streq(char* s1, char* s2);
int string_is_number(char* s);
int string_value(char* s);
int strlength(char* string);
char* number_to_string(char number,char* string);
unsigned char getMouseX();
unsigned char getMouseY();
unsigned char getLMouseButton();
unsigned char getRMouseButton();
unsigned char getMMouseButton();
unsigned char getCharAt(int x,int y);
void writeAt(int x,int y, int character);
