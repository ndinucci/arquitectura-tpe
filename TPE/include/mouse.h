
typedef struct{
	unsigned char x;
	unsigned char y;
	unsigned char r_button_pressed;
	unsigned char l_button_pressed;
	unsigned char m_button_pressed;
}Mouse;

void initialize_mouse();
void mouse_write(unsigned char a_write);
void mouse_wait(unsigned char a_type);
unsigned char mouse_read();
