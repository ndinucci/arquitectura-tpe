
#define SHELL_BUFFER_SIZE	256
#define COMMANDS		6
#define HELP			0
#define TESTMOUSE		1
#define GETTIME			2
#define SETTIME			3
#define CLEAR			4
#define SETSCREENSAVER		5


typedef struct{
	char index;
	char buffer[SHELL_BUFFER_SIZE];
} Shell;

void show_shell();
void initialize_shell();
