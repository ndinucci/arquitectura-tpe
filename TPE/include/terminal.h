
#define TERMINAL_BUFFER_SIZE	256

typedef struct{
	int index;
	char buffer[TERMINAL_BUFFER_SIZE];
} Terminal;

int terminal_read(char* buffer,int size);
void deleteCharacter();
void newLine();
void showCharacter(char aux);
