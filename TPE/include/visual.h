#define SCREEN_START	0xb8000
#define WIDTH		80
#define HEIGHT		25
#define SCREEN_SIZE	HEIGHT*WIDTH*2
#define WHITE_TXT 	0x07
#define CURSOR_FMT	0x70

typedef struct{
	int cursor;
	char *vidmem;
	char screen[SCREEN_SIZE];
	char mousescreen[SCREEN_SIZE];
	char screensaver[SCREEN_SIZE];
	int active;
}Visual;

void initialize_screen();
void show_character(char aux);
void new_line();
void delete_character();
int write_string(char* buffer,int size);
void goInactive();
void goActive();
void go_test_mouse();
void leave_test_mouse();
void mouse_cursor(int prev_x,int prev_y,int new_x, int new_y);
int visual_char_at(int x,int y);
void write_at(int x, int y,int character);

