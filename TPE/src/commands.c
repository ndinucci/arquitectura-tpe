#include "../include/libc.h"
#include "../include/visual.h"
#include "../include/rtc.h"
#include "../include/testMouse.h"
#include "../include/shell.h"

extern Shell shell;

void help(){
	printf("Has ingresado el comando 'help'. Los comandos validos son:");
	printf("     help");
	printf("     clear");
	printf("     getTime");
	printf("     setTime");
	printf("     setScreensaver");
	printf("     testMouse");
}

void test_mouse(){
	_system_call(SYS_CHMODE,MOUSE_MODE,NULL,NULL);
	in_test_mouse();
	_system_call(SYS_CHMODE,KEYBOARD_MODE,NULL,NULL);
}

void get_time(){
	rtcGet(shell.buffer);
	printf(shell.buffer);
}

void set_time(){
	int horas=-1,minutos=-1,segundos=-1;
	do{
		printf("Ingrese la hora: ");		
		bring_command(shell.buffer,SHELL_BUFFER_SIZE);
		if(string_is_number(shell.buffer)){
			horas=string_value(shell.buffer);
		}
	}while(horas<0 || horas>23);
	do{
		printf("Ingrese los minutos: ");		
		bring_command(shell.buffer,SHELL_BUFFER_SIZE);
		if(string_is_number(shell.buffer)){
			minutos=string_value(shell.buffer);
		}
	}while(minutos<0 || minutos>59);
	do{
		printf("Ingrese los segundos: ");		
		bring_command(shell.buffer,SHELL_BUFFER_SIZE);
		if(string_is_number(shell.buffer)){
			segundos=string_value(shell.buffer);
		}
	}while(segundos<0 || segundos>59);
	rtcSet(horas,minutos,segundos);
}

void clear(){
	clear_sys();
}

void set_screensaver(){
	unsigned int time=0;
	do{
		printf("Ingrese tiempo de espera para inactividad: ");		
		bring_command(shell.buffer,SHELL_BUFFER_SIZE);
		if(string_is_number(shell.buffer)){
			time=string_value(shell.buffer);
		}
	}while(time<=0||time>=65536);
	_system_call(SYS_SCRSV, time, NULL, NULL);
}
