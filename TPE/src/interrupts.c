#include "../include/kasm.h"
#include "../include/keyboard.h"
#include "../include/visual.h"

int mode=0;
int inactive=0;
int screensaverTime=20;
int isActive=1;


void int_08() {
	if(inactive<screensaverTime*TTICKS_PER_SECOND){
		inactive++;	
	}
	else{
		if(isActive){
			goInactive();
			isActive=0;
		}
	}
}

void int_09(short int scancode){
	if(mode==KEYBOARD_MODE){
		process_scancode(scancode);
	}
	inactive = 0;
	if(!isActive){
		goActive();
		isActive=1;
	}
}

void int_74(){
	inactive = 0;
	if(!isActive){
		goActive();
		isActive=1;
	}
	if(mode==MOUSE_MODE){
		refresh_mouse();
	}
}

int int_80(int sysnum, void* arg1, void* arg2, void* arg3){
	switch(sysnum){
		case SYS_READ:{
			return _read(arg1, arg2, arg3);
			break;
		}
		case SYS_WRITE:{
			return _write(arg1, arg2, arg3);
			break;
		}
		case SYS_WRITE_AT:{
			write_at((int)arg1,(int)arg2,(int)arg3);
			return 1;
			break;
		}
		case SYS_KEYBOARD:{
			return can_read();
			break;
		}
		case SYS_TERM:{
			_terminal((int)arg1,(int)arg2);
			return 1;
			break;
		}
		case SYS_PROMPT:{
			print_prompt();
			return 1;
			break;
		}
		case SYS_RTC_GET:{
			rtc_get((char*) arg1);
			return 1;
			break;
		}
		case SYS_RTC_SET:{
			rtc_set((int)arg1,(int)arg2,(int)arg3);
			return 1;
			break;
		}
		case SYS_SCRSV:{
			screensaverTime=(int)arg1;
			return 1;
			break;
		}
		case SYS_CHMODE:{
			mode=(int)arg1;
			if(mode==MOUSE_MODE){
				go_test_mouse();
			}
			else{
				leave_test_mouse();
			}
			return 1;
			break;
		}
		case SYS_CLRSCRN:{
			clear_screen();
			return 1;
			break;
		}
		default:{
			return -1;
		}
	}
}

