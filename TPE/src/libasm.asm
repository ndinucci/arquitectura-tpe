GLOBAL  _read_msw,_lidt
GLOBAL  _int_08_hand,_int_09_hand,_int_74_hand,_int_80_hand
GLOBAL	_system_call
GLOBAL  _mascaraPIC1,_mascaraPIC2,_Cli,_Sti
GLOBAL  _debug
GLOBAL	_outb, _inb
EXTERN  int_08,int_09,int_74,int_80


SECTION .text


_Cli:
	cli			; limpia flag de interrupciones
	ret

_Sti:

	sti			; habilita interrupciones por flag
	ret

_mascaraPIC1:			; Escribe mascara del PIC 1
	push    ebp
        mov     ebp, esp
        mov     ax, [ss:ebp+8]  ; ax = mascara de 16 bits
        out	21h,al
        pop     ebp
        retn

_mascaraPIC2:			; Escribe mascara del PIC 2
	push    ebp
        mov     ebp, esp
        mov     ax, [ss:ebp+8]  ; ax = mascara de 16 bits
        out	0A1h,al
        pop     ebp
        retn

_read_msw:
        smsw    ax		; Obtiene la Machine Status Word
        retn


_lidt:				; Carga el IDTR
        push    ebp
        mov     ebp, esp
        push    ebx
        mov     ebx, [ss: ebp + 6] ; ds:bx = puntero a IDTR 
	rol	ebx,16		    	
	lidt    [ds: ebx]          ; carga IDTR
        pop     ebx
        pop     ebp
        retn


_int_08_hand:				; Handler de INT 8 (Timer tick)
        pusha                           ; Carga de DS y ES con el valor del selector                 
        call    int_08                 
        mov	al,20h			; Envio de EOI generico al PIC
	out	20h,al
	popa                            
        iret


_int_09_hand:
	pusha				; Handler de INT 9 (Teclado)
	in	ax,60h  
	and	ax,255          
	push	ax
        call    int_09  
	pop	ax
	mov	al,20h			; Envio de EOI generico al PIC
	out	20h,al
	popa
        iret

_int_74_hand:
	pusha				; Handler de INT 74 (Mouse)
	in al, 60h
	push eax
        call    int_74
	pop eax
	mov	bl,al
	mov	al,20h			; Envio de EOI generico al PIC
	out	0A0h, al 		; y al slave
	out	20h, al
	mov	al, bl
	popa
        iret

_int_80_hand:			
	push	edx			; Handler de INT80
	push	ecx
	push	ebx
	push	eax
        call    int_80
	pop	ebx			; No "popeo" en eax para no perder el retorno de la funcion
	pop	ebx
	pop	ebx
	pop	ebx 
	mov	bl, al
        mov	al, 20h			; Envio de EOI generico al PIC
	out	20h, al
	mov	al, bl     
        iret

_system_call:
	push ebp
	mov ebp, esp
	mov eax,[ebp+8]
	mov ebx,[ebp+12]
	mov ecx,[ebp+16]
	mov edx,[ebp+20]
	int 80h
	mov esp,ebp
	pop ebp
	ret	

_outb:
	push ebp
	mov ebp, esp
	cli
	mov eax,[esp+12]
	mov edx,[esp+8]
	out dx, al
	sti
	mov esp, ebp
	pop ebp
	ret

_inb:
	push ebp
	mov ebp, esp
	cli
	mov edx,[esp+8]
	in al, dx
	sti
	mov esp, ebp
	pop ebp
	ret



; Debug para el BOCHS, detiene la ejecución; Para continuar colocar en el BOCHSDBG: set $eax=0
;


_debug:
        push    bp
        mov     bp, sp
        push	ax
vuelve:	mov     ax, 1
        cmp	ax, 0
	jne	vuelve
	pop	ax
	pop     bp
        retn
