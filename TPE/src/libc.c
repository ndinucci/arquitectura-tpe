#include "../include/kc.h"
#include "../include/defs.h"
#include "../include/libc.h"




void rtcSet(int horas, int minutos, int segundos){
	_system_call(SYS_RTC_SET, horas,(void*) minutos, segundos);
	return;
}
void rtcGet(char* buffer){
	_system_call(SYS_RTC_GET, buffer,(void*) buffer, buffer);
	return;
}
unsigned char getCharAt(int x,int y){
	return (unsigned char)read(CHAR_AT,(void*)x,y);
}

void writeAt(int x,int y, int character){
	_system_call(SYS_WRITE_AT,x,y,character);
	return;
}

void clear_sys(){
	_system_call(SYS_CLRSCRN, 0, (void*)0, 0);
}

unsigned char getMouseX(){
	return read(ST_MOUSE,(void*)GET_X,GET_X);
}

unsigned char getMouseY(){
	return read(ST_MOUSE,(void*)GET_Y,GET_Y);
}

unsigned char getLMouseButton(){
	return read(ST_MOUSE,(void*)GET_L_BOT,GET_L_BOT);
}
unsigned char getRMouseButton(){
	return read(ST_MOUSE,(void*)GET_R_BOT,GET_R_BOT);
}
unsigned char getMMouseButton(){
	return read(ST_MOUSE,(void*)GET_M_BOT,GET_M_BOT);
}

int read(int fd, void* buffer, int size){
	return _system_call(SYS_READ, fd, buffer, size);
}

int write(int fd, void* buffer, int size){
	return _system_call(SYS_WRITE, fd, buffer, size);
}

int scanf(char* buffer, int size){
	bring_command(buffer,size);
	return 1;
}  

int canRead(){
	return _system_call(SYS_KEYBOARD, 0,(void*) 0, 0);
}

char getchar(){
	char aux;
	while(!canRead());
	read(STDIN, &aux, 1);
	return aux;
}

void putc(char c){
	write(STDOUT, &c, 1);
}

void printf(char* string){
	write(STDOUT, string, strlength(string));
}

void printPrompt(){
	_system_call(SYS_PROMPT,0,0,0);	
	return;
}

int streq(char* s1, char* s2){
	int i;
	for(i=0;s1[i]!='\0' && s2[i]!='\0';i++){
		if(s1[i]!=s2[i]){
			return FALSE;
		}
	}
	return (s1[i]=='\0' && s2[i]=='\0')?TRUE:FALSE; 
	
}

int string_is_number(char* s){
	int i=0;
	if(s[0]=='\0'){
		return FALSE;
	}	
	if(s[0]=='-')
		i++;
	for(;s[i]!='\0';i++){
		if(s[i]<'0'||s[i]>'9')
			return FALSE;
	}
	return TRUE;	
}

char* number_to_string(char number,char* string){
	int i=0;	
	if(number==0){
	string[0]='0';
	string[1]=0;
	}
	string[9]=0;
	while(number!=0){
		string[8-i]='0'+number%10;
		number=number/10;
		i++;
	}
	return string+9-i;
}

int string_value(char* s){
	int ans=0;
	int i=0;
	int neg=0;
	if(s[0]=='-'){
		neg=1;
		i++;
	}
	for(;s[i]!='\0';i++){
		ans*=10;
		ans+=s[i]-'0';
	}
	return neg==1?-ans:ans;
}


int strlength(char* string){
	int i=0;
	while(*(string+i)!=0){
		i++;
	}
	return i;
}



