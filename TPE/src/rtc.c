

void rtc_set(unsigned char horas,unsigned char minutos,unsigned char segundos){
	
	_outb(0x70,04);
	_outb(0x71,horas);
	
	_outb(0x70,02);
	_outb(0x71,minutos);

	_outb(0x70,00);
	_outb(0x71,segundos);
}

void rtc_get(char* buffer){
	unsigned char horas,minutos,segundos,i;
	_outb(0x70,04);
	horas=_inb(0x71);

	_outb(0x70,02);
	minutos=_inb(0x71);

	_outb(0x70,00);
	segundos=_inb(0x71);

	for(i=0;i<8;i++){
		buffer[i]='0';
	}

	number_to_string(horas,buffer-7);
	number_to_string(minutos,buffer-4);
	number_to_string(segundos,buffer-1);
	buffer[2]=':';
	buffer[5]=':';
}




void initialize_rtc(){
	unsigned char aux;
	_outb(0x70,0x0B);
	aux=_inb(0x71);
	aux=aux | 0x06;
	_outb(0x70,0x0B);
	_outb(0x71,(char) aux);

}
