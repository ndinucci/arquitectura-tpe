#include "../include/shell.h"
#include "../include/commands.h"



Shell shell;

char* commands[]={"help","testMouse","getTime","setTime","clear","setScreensaver"};

void initialize_shell(){
	shell.index = 0;
}




int get_command(){
	int i;	
	for(i=0;i<COMMANDS;i++){
		if(streq(commands[i],shell.buffer)){
			return i;		
		}	
	}
	return -1;
}

void run_command(int command){
	switch(command){
		case HELP:{
			help();
			break;
		}
		case TESTMOUSE:{
			test_mouse();
			break;
		}
		case GETTIME:{
			get_time();
			break;
		}
		case SETTIME:{
			set_time();
			break;
		}
		case CLEAR:{
			clear();
			break;
		}
		case SETSCREENSAVER:{
			set_screensaver();
			break;
		}
		default:{
			printf("Comando invalido, si necesita ayuda use el comando 'help'.");
			break;
		}
	}
	return;
}


void show_shell(){
	int command;
	while(1){
		printPrompt();
		bring_command(shell.buffer, SHELL_BUFFER_SIZE);
		command=get_command();
		run_command(command);
	}
}


