#include "../include/defs.h"
#include "../include/visual.h"
#include "../include/keyboard.h"

int _read(int fd, void* buffer, int size){
	switch(fd){
		case STDIN:{
			return character_read((char*)buffer);
			break;
		}
		case ST_MOUSE:{
			return visual_mouse_read(buffer);
			break;
		}
		case CHAR_AT:{
			return visual_char_at((int)buffer,(int)size);
			break;
		}
		default:{
			break;
		}
	}
	return 1;
}

int _write(int fd, char* buffer, int size){
	int i;
	switch(fd){
		case STDOUT:{
			write_string(buffer,size);	
			break;
		}
		default:{
			break;
		}
	}
	return 1;
}

void _terminal(int fd, char aux){
	switch(fd){
		case DELETE:{
			delete_character();	
			break;
		}
		case NEW_LINE:{
			new_line();	
			break;
		}
		case SHOW_CHAR:{
			show_character(aux);	
			break;
		}
		default:{
			break;
		}
	}
	return;
}
