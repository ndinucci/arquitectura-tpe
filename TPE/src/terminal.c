#include "../include/visual.h"
#include "../include/terminal.h"
#include "../include/keyboard.h"
#include "../include/defs.h"

Terminal terminal;

void bring_command(char* buffer,int size){
	int j;
	terminal.index=0;
	while(terminal.index<size-2){
		char aux=getchar();			
		switch(aux){
			case BACKSPACE :{
				if(terminal.index!=0){
					deleteCharacter();
					terminal.index--;					
				}
				break;				
			}
			case ENTER :{
				newLine();
				for(j=0 ;j<terminal.index ;j++){
					buffer[j]=terminal.buffer[j];
				}
				buffer[j]=0;				
				return;
				break;
			}
			default :{
				terminal.buffer[terminal.index]=aux;
				showCharacter(aux);
				terminal.index++;
				break;
			}
			
		}
	}
	new_line();
	for(j=0 ;j<terminal.index-1 ;j++){
		buffer[j]=terminal.buffer[j];
	}
	buffer[j-1]=0;
	return;
}


void deleteCharacter(){
	_system_call(SYS_TERM, DELETE, (void*)0, 0);
	return;
}
void newLine(){
	_system_call(SYS_TERM, NEW_LINE, (void*)1, 1);
	return;
}
void showCharacter(char character){
	int aux=character;
	_system_call(SYS_TERM, SHOW_CHAR, (void*)aux, 0);	
	return;
}








