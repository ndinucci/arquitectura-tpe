#include "../include/libc.h"
#include "../include/shell.h"
#include "../include/testMouse.h"


int belongs_button(int x, int y){
	if(x>=35 && x<=45 && y>=17 && y<=19 ){
		return EXIT;
	}
	if(x>=46 && x<=48 && y>=9 && y<=11 ){
		return SUM;
	}
	if(x>=32 && x<=34 && y>=9 && y<=11 ){
		return SUB;
	}
	return NOTHING;
}

void sum(){
	char aux=getCharAt(NUMBER_X,NUMBER_Y);
	if(aux<'9'){
		writeAt(NUMBER_X,NUMBER_Y,aux+1);
	}
}

void sub(){
	char aux=getCharAt(NUMBER_X,NUMBER_Y);
	if(aux>'0'){
		writeAt(NUMBER_X,NUMBER_Y,aux-1);
	}
}

void capNumber(){
	writeAt(NUMBER_X,NUMBER_Y,'9');
}

void resetNumber(){
	writeAt(NUMBER_X,NUMBER_Y,'0');
}

void showCoordenates(int x,int y){
	writeAt(71,24,'X');
	writeAt(72,24,':');
	writeAt(73,24,(x/10)+'0');
	writeAt(74,24,(x%10)+'0');
	writeAt(75,24,' ');
	writeAt(76,24,'Y');
	writeAt(77,24,':');
	writeAt(78,24,(y/10)+'0');
	writeAt(79,24,(y%10)+'0');
}

void in_test_mouse(){
	int x,y,r_button,l_button,m_button;
	while(1){
		l_button = getLMouseButton();
		r_button = getRMouseButton();
		m_button = getMMouseButton();
		x = getMouseX();
		y = getMouseY();
		showCoordenates(x,y);
		if(l_button){
			switch(belongs_button(x,y)){
				case EXIT:{
					return;
					break;
				}
				case SUM:{
					sum();
					break;
				}
				case SUB:{
					sub();
					break;
				}
				default:{
					break;
				}
			}
			
		}
		if(r_button){
			capNumber();
		}
		if(m_button){
			resetNumber();
		}
	}
}
